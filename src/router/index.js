import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import DishDetails from '../views/DishDetails.vue'
import AboutView from '../views/AboutView.vue'
import AddDish from '../views/AddDish.vue'
import EditView from '../views/EditView.vue'
import Login from '../views/Login.vue'



const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/dish/:id',
    name: 'dish-details',
    component: DishDetails
  },
  {
    path: '/about',
    name: 'about',
    component: AboutView

  },
  {
    path: '/new',
    name: 'new-dish',
    component: AddDish

  },
  {
    path: '/edit/:id',
    name: 'edit-dish',
    component: EditView

  },
  {
    path: '/login',
    name: 'login',
    component: Login

  },

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
