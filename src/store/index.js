import { createStore } from 'vuex'
import dishes from './module/dishes'
import auth from './module/auth'
import users from './module/users'

export default createStore({
    namespaced: true,
    modules: {
        dishes,
        auth,
        users,
    }
})