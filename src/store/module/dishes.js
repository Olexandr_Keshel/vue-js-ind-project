import getModuleSettingsObject from "../helpers/GetModuleSettingsObject"

export default {
  ...getModuleSettingsObject('dishes')
}