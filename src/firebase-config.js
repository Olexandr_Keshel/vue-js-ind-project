// Import the functions you need from the SDKs you need
import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore'
import { getAuth} from 'firebase/auth';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyB7J6YmG2f5rfWUHNuo_8fRu_ifQFhk7L4",
  authDomain: "ind-proj-international-cuisine.firebaseapp.com",
  projectId: "ind-proj-international-cuisine",
  storageBucket: "ind-proj-international-cuisine.appspot.com",
  messagingSenderId: "203760738549",
  appId: "1:203760738549:web:5874434264528e2084a5bc"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const db = getFirestore(app)

export const auth = getAuth(app) 

export default db